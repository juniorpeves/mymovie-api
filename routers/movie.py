from fastapi import APIRouter
from fastapi import Depends, Path, Query
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
# encoders sirve para convertir el resultado en un objeto(json)
from fastapi.encoders import jsonable_encoder
# Importando la clase desde el middlewares
from middlewares.jwt_bearer import JWTBearer
# Importando el servicio de movie
from services.movie import MovieService
from schema.movie import Movie

movie_router = APIRouter()

@movie_router.get('/movies', 
    tags=['movies'], 
    response_model=List[Movie], 
    status_code= 200,
    dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code= 200, content=jsonable_encoder(result))

#Recibe el parametro id en el path
#Usa el id como entero, pide un minimo de 1 y maximo de 2000
@movie_router.get('/movies/{id}', 
    tags=['movies'], 
    response_model=Movie, 
    status_code=200) 
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie: 
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message" : "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#No recibe el parametro en el path
#Parámetros Query dentro de la función, str entre 5 a 15 de largo.
@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie]) 
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies_by_category(category)
    return JSONResponse(status_code= 200, content= jsonable_encoder(result))

# Metodo POST, para hacer CREATE
# Se crea sesión, luego se pasa la información (en dict) al modelo,
# luego la variable se agrega a db, y finalmente el commit(Actualización)
@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code= 201)
def create_movie(movie: Movie) -> dict:
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code= 201, content={"message":"Se ha registrado la pelicula"})

# Metodo PUT, para hacer UPDATE
# Recibe el parametro id en el path
@movie_router.put("/movies/{id}",tags=["movies"], response_model=dict, status_code= 200) 
# Parámetros Query dentro de la función
def update_movie(id:int, movie:Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message" : "No encontrado"})
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code= 200, content={"message":"Se ha modificado la pelicula"}) 

# Metodo DELETE, para hacer DELETE
@movie_router.delete('/movies/{id}', tags=["movies"], response_model=dict)
def delete_movie(id : int) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message" : "No encontrado"})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code= 200, content={"message":"Se ha eliminado la pelicula"}) 