## Para vincular tu cuenta ssh con gitlab es necesario configurar la SSH key:

1. Primero debes ubicar tu clave publica en consola con ``cd ~/.ssh/`` , luego ``ls``, debes ubicar el archivo *.pub, abrir con algun editor y copiar el contenido.
2. En tu cuenta de gitlab debes ir a ``User Setting / SSH Keys`` y crear una nueva key, agregar lo copiado y guardar.
3. Para validar tu conexión debes vovler a la consola y ejecutar ``ssh -T git@gitlab.com``

[Generar ssh](https://www.notion.so/juniorpeves/Generar-una-nueva-clave-SSH-y-agregarla-al-ssh-agent-d6eed290b3fa476f86e4ab1402bc6b46?pvs=4#9d7c287c545a424b88347f8a78483180)